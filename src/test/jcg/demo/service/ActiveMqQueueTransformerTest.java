package jcg.demo.service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import jcg.demo.model.ActiveMqQueue;
import jcg.demo.model.ListOfActiveMqQueue;
import jcg.demo.model.Stats;

public class ActiveMqQueueTransformerTest {

	private ActiveMqQueueTransformer transformer = new ActiveMqQueueTransformer();

	@Test
	public void convertFromInputStream() throws FileNotFoundException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("activemqQueues.xml").getFile());

		ListOfActiveMqQueue queues = transformer.convertFromInputStream(new FileInputStream(file));

		assertNotNull(queues);
		assertEquals(1, queues.getQueue().size());
		assertEquals("test.queue", queues.getQueue().get(0).getName());
	}

}
