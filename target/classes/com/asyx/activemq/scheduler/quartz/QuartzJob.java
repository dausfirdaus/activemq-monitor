package com.asyx.activemq.scheduler.quartz;

import java.time.LocalDateTime;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.asyx.activemq.service.ActiveMQMonitorService;

/**
 * This class implements Quartz Job interface, anything you wish to be executed
 * by the Quartz Scheduler should be here it should invokes business class to
 * perform task.
 * 
 * @author Mary.Zheng
 *
 */
public class QuartzJob implements Job {
    private static final Logger LOG = LoggerFactory
            .getLogger(QuartzJob.class);

    private static final String LOGIN = "admin";
    private static final int MAX_PENDING_MESSAGE_SIZE_LIMIT = 10;
    private String brokerXmlUrl = "http://localhost:8161/admin/xml/queues.jsp";

    private ActiveMQMonitorService activeMqMonitorService = new ActiveMQMonitorService();

    @Override
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        LocalDateTime localTime = LocalDateTime.now();
        LOG.info("{}: Run QuartzJob at {}", Thread.currentThread().getName(),
                localTime.toString());

        activeMqMonitorService.monitorAndNotify(brokerXmlUrl, LOGIN, LOGIN,
                MAX_PENDING_MESSAGE_SIZE_LIMIT);
    }
}
