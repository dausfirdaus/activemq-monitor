package com.asyx.activemq.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.asyx.activemq.model.ActiveMqQueue;
import com.asyx.activemq.model.ListOfActiveMqQueue;

public class ActiveMQMonitorService {
    private static final Logger LOG = LoggerFactory
            .getLogger(ActiveMQMonitorService.class);

    private ActiveMqQueueTransformer transformer = new ActiveMqQueueTransformer();

    public void monitorAndNotify(String brokerUrl, String username,
            String password, int maxPendingMessageLimit) {
        LocalDateTime now = LocalDateTime.now();
        LOG.info("timestamp[{}]: monitorAndNotify starts for {}", now,
                brokerUrl);
        StringBuilder body = new StringBuilder();
        InputStream xmlFeedData = readXmlFeeds(brokerUrl, username, password);

        ListOfActiveMqQueue activeMqXmlData = transformer
                .convertFromInputStream(xmlFeedData);
        if (activeMqXmlData != null) {
            for (ActiveMqQueue queue : activeMqXmlData.getQueue()) {
                if (queue.getStats().getSize() > maxPendingMessageLimit
                        && queue.getStats().getConsumerCount() == 0
                        && !queue.getName().toLowerCase().contains("dlq")) {
                    body.append("QUEUE : " + queue.getName()
                            + " has large pending message ")
                            .append("and has no consumer.")
                            .append("\n");
                }
            }
            if (body.length() > 0)
                LOG.error("timestamp[{}]: Must check activeMQ, {}", now,
                        body.toString());
        }
        LOG.info("timestamp[{}]: monitorAndNotify completes for {}", now,
                brokerUrl);
    }

    private InputStream readXmlFeeds(String brokerUrl, String username,
            String password) {
        try {
            URL url = new URL(brokerUrl);
            URLConnection uc = url.openConnection();

            String userpass = username + ":" + password;
            String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter
                    .printBase64Binary(userpass.getBytes());

            uc.setRequestProperty("Authorization", basicAuth);

            return uc.getInputStream();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
